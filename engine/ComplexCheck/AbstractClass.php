<?php
namespace TkachInc\SituationalEngine\ComplexCheck;

use TkachInc\SituationalEngine\SituationUser;

/**
 * Класс позволяющий проводить сложные комбинированые проверки
 */
abstract class AbstractClass
{
	/**
	 * @var array Конфигурация события
	 */
	private $_config;

	/**
	 * Получает конфигурацию по ссылке
	 *
	 * @param $config
	 */
	public function __construct(&$config)
	{
		$this->_config = &$config;
	}

	/**
	 * Метод возвращающий конфигурацию по ссылке
	 *
	 * @return mixed
	 */
	public function &getConfig()
	{
		return $this->_config;
	}

	/**
	 * Метод проверки попадает ли пользователь под данное событие
	 *
	 * @param SituationUser $user
	 * @return bool
	 */
	abstract function isAvailable(SituationUser $user);
}