<?php
namespace TkachInc\SituationalEngine;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Ситуативное ядро. Содержит методы определения, попадает пользователь под условия, или нет.
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SituationUser
{
	protected $id;
	protected $pk;
	protected $object;

	/**
	 * @param ObjectModel $user
	 */
	public function __construct(ObjectModel $user)
	{
		$pk = $user::getPK();
		$this->id = $user->{$pk};
		$this->pk = $pk;
		$this->object = $user;
	}

	/**
	 * @return ObjectModel
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}
}