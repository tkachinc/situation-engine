<?php
namespace TkachInc\SituationalEngine;

use TkachInc\Core\Database\MongoDB\Helper\MongoConditionChecker;
use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\SituationalEngine\ComplexCheck\AbstractClass;

/**
 * Ситуативное ядро. Содержит методы определения, попадает пользователь под условия, или нет.
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SituationCore
{
	protected static $cacheSimpleCheck = [];

	/**
	 * Метод производящий выбор пользовтаеля, и его приоритета
	 *
	 * @param  SituationUser $user
	 * @param BaseSituationConfig $model
	 * @param array $query
	 * @param array $params
	 * @param bool $simple
	 * @return array
	 */
	public static function getSimpleCheck(SituationUser $user,
	                                      BaseSituationConfig $model,
	                                      Array $query = [],
	                                      Array $params = [],
	                                      $simple = false)
	{
		$config = $model::getAllAdvanced($params, [], false, true);
		foreach ($config as $key => &$value) {
			if (!empty($value['class']) && class_exists($value['class'])) {
				$checkClassName = $value['class'];
				/** @var AbstractClass $promoClass */
				$promoClass = new $checkClassName($value);
				if (!$promoClass->isAvailable($user)) {
					unset($config[$key]);
				}
			}

			if (!empty($value['models'])) {
				foreach ($value['models'] as $model => $params) {
					if ($simple === false && !empty($model) && class_exists(
							$model
						) && isset($params['params']) && isset($params['pk']) && is_array($params['params'])
					) {
						//						var_dump($params);
						$params['params'] = array_replace(
							$params['params'],
							array_intersect_key($query, $params['params'])
						);
						if (!static::mongoConditionCheck($user->getId(), $model, $params['params'], $params['pk'])) {
							unset($config[$key]);
						}
					} elseif (isset($params['params'])) {
						if (!static::mongoConditionCheckByObject($user, $params['params'])) {
							unset($config[$key]);
						}
					}
				}
			}
		}
		unset($value);

		//reindexed array returned
		return array_values($config);
	}

	/**
	 * @param  SituationUser $user
	 * @param BaseSituationConfig $model
	 * @param                     $params
	 * @param bool $simple
	 * @return array
	 */
	public static function oneSituationCheck(SituationUser $user, BaseSituationConfig $model, $params, $simple = false)
	{
		$value = $model::findOne($params, []);
		if (!empty($value['class']) && class_exists($value['class'])) {
			$checkClassName = $value['class'];
			/** @var AbstractClass $promoClass */
			$promoClass = new $checkClassName($value);
			if (!$promoClass->isAvailable($user)) {
				return [];
			}
		}

		if (!empty($value['models'])) {
			foreach ($value['models'] as $model => $params) {
				if ($simple === false && !empty($model) && class_exists(
						$model
					) && isset($params['params']) && isset($params['pk']) && is_array($params['params'])
				) {
					if (!static::mongoConditionCheck($user->getId(), $model, $params['params'], $params['pk'])) {
						return [];
					}
				} elseif (isset($params['params'])) {
					if (!static::mongoConditionCheckByObject($user, $params['params'])) {
						return [];
					}
				}
			}
		}

		//reindexed array returned
		return $value;
	}

	/**
	 * @param array $callback
	 * @param array $attributes
	 */
	public static function callback(Array $callback, Array $attributes)
	{
		if (is_callable($callback)) {
			call_user_func_array($callback, [$attributes]);
		}
	}

	/**
	 * Проверка запроса по полученому объекту
	 *
	 * @param        $userId
	 * @param string $model
	 * @param array $params
	 * @param string $pk
	 * @return bool
	 */
	public static function mongoConditionCheck($userId, $model = '', Array $params = [], $pk = '')
	{
		if (empty($pk)) {
			// Получаем название PK, модели БД
			/** @var ObjectModel $model */
			$pk = $model::getPK();
		}

		$paramsData[$pk] = $userId;
		$obj = new $model($paramsData);

		return MongoConditionChecker::evaluate($params, $obj);
	}

	/**
	 * Проверка запроса по полученому объекту
	 *
	 * @param SituationUser $user
	 * @param array $params
	 * @return bool
	 */
	public static function mongoConditionCheckByObject(SituationUser $user, Array $params = [])
	{
		return MongoConditionChecker::evaluate($params, $user->getObject());
	}
}