<?php
namespace TkachInc\SituationalEngine;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property string _id
 * @property string status
 * @property string type
 * @property int duration
 * @property string transition
 * @property int priority
 * @property bool enabled
 * @property bool node
 * @property string next_status
 * @property string prev_status
 * @property int min_used
 * @property int max_used
 * @property int max_back
 * @property string class
 * @property string model
 * @property array params
 * @property array attributes
 * @property array callback
 * @property string comments
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseSituationConfig extends ObjectModel
{
	/** @var string $pk первичный ключ */
	protected static $_pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $_sort = ['enabled' => -1, 'priority' => -1];
	/** @var array $indexes индексы коллекции */
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['name' => 1, 'enabled' => -1, 'date_to' => 1],
		],
		[
			'keys' => ['enabled' => -1, 'priority' => -1],
		],
		[
			'keys' => ['enabled' => -1, 'date_to' => 1],
		],
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $_fieldsDefault = [
		'_id'        => '',
		'name'       => '',
		'type'       => '',
		'priority'   => 0,
		'enabled'    => false,
		'class'      => '',
		'models'     => [],
		'attributes' => [],
		'callback'   => [],
		'date_to'    => 0,
		'date_from'  => 0,
		'comments'   => '',
	];

	/** @var array $fieldsValidate типы значений для валидации */
	protected static $_fieldsValidate = [
		'_id'        => self::TYPE_MONGO_ID,
		'name'       => self::TYPE_STRING,
		'type'       => self::TYPE_STRING,
		'priority'   => self::TYPE_UNSIGNED_INT,
		'enabled'    => self::TYPE_BOOL,
		'class'      => self::TYPE_STRING,
		'models'     => self::TYPE_JSON,
		'attributes' => self::TYPE_JSON,
		'callback'   => self::TYPE_JSON,
		'date_to'    => self::TYPE_TIMESTAMP,
		'date_from'  => self::TYPE_TIMESTAMP,
		'comments'   => self::TYPE_STRING,
	];

	protected static $_fieldsHint = [
		'_id'        => 'Идентификатор БД',
		'name'       => 'Название тригера',
		'type'       => 'Тип тригера',
		'priority'   => 'Приоритет (нужен для сортировки, и последовательной выборки)',
		'enabled'    => 'Включатель тригеров',
		'class'      => 'Клас проверки (может совершать подмену данных, при необходимости)',
		'models'     => 'Модель для проверки. Массив содержащий ключ - модель БД, в массиве params - запрос к БД, и pk - первичный ключ коллекции (тот что содержит идентификатор пользователя). Также можно добавить "simple": true, тогда будет делаться запрос, если нет, создавать модель)',
		'attributes' => 'Атрибуты передаваемые в callback',
		'callback'   => 'Класс и метод, который необходимо дернуть, для активации',
		'date_to'    => 'Время старта тригера',
		'date_from'  => 'Время завершения тригера',
		'comments'   => 'Коментарий разработчика',
	];
} 