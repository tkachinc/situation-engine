<?php
namespace TkachInc\SituationSale\Classes;

use TkachInc\SituationSale\UserActionInterface;
use TkachInc\SituationalEngine\SituationUser;
use TkachInc\SituationSale\Model\SaleModel;
use TkachInc\SituationalEngine\SituationCore;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Sales
{
	/**
	 * @param SaleModel $model
	 * @param UserActionInterface $user
	 * @return null|Sale
	 */
	public static function getSale(SaleModel $model, UserActionInterface $user)
	{
		$action = null;
		if (empty($user->promo_name)) {
			$params = [
				'enabled' => true,
				'$or'     => [['date_to' => ['$gte' => time()]], ['date_to' => 0]],
			];
			$data = SituationCore::getSimpleCheck(new SituationUser($user), $model, [], $params, true);
			$data = array_reverse($data);
			foreach ($data as $item) {
				$actionModel = new SaleModel(['name' => $item['name']]);
				$action = new Sale($actionModel, $user, true);
				break;
			}
		} else {
			$actionModel = new SaleModel(['name' => $user->promo_name]);
			$action = new Sale($actionModel, $user);
		}

		return $action;
	}

	/**
	 * Возвращаем конкретную акцию игрока. С пред и пост проверкой на меню покупок и количество потраченных денег
	 *
	 * @param SaleModel $model
	 * @param UserActionInterface $user
	 * @param bool $reSelect
	 * @return Sale|null
	 */
	public static function selectUserSale(SaleModel $model, UserActionInterface $user, $reSelect = false)
	{
		$action = static::getSale($model, $user);

		if ($reSelect) {
			$user->promo_name = '';
			$user->promo_used_menu = 0;
			$action = static::getSale($model, $user);
		}

		return $action;
	}

	/**
	 * @param Sale $action
	 * @param UserActionInterface $user
	 */
	public static function checkUserSale(Sale $action, UserActionInterface $user)
	{
		if ($action->isActivate()) {
			if ($action->isComplete()) {
				$loop = false;
				if ($action->isLoop()) {
					$loop = true;
					$user->promo_name = $action->getNextStep($loop);
				} elseif ($action->isNext()) {
					$user->promo_back = 0;
					$user->promo_name = $action->getNextStep();
				} elseif ($action->isPrev()) {
					$user->promo_back++;
					$user->promo_name = $action->getNextStep();
				}
				$action->deactivate($loop);

				if ($action->isNextSale()) {
					$actionModel = new SaleModel();
					$action = static::getSale($actionModel, $user);
					$action->activate();
				}
			}

			return;
		} else {
			if ($action->isNextSale()) {
				$action->activate();
			}

			return;
		}
	}
}