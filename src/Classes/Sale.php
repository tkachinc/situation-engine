<?php
namespace TkachInc\SituationSale\Classes;

use TkachInc\SituationSale\UserActionInterface;
use TkachInc\SituationalEngine\SituationUser;
use TkachInc\SituationSale\Model\SaleModel;
use TkachInc\SituationalEngine\SituationCore;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Sale
{
	protected $model;
	protected $user;
	protected $nextStep = null;
	protected $currentTime;

	/**
	 * @param SaleModel $model
	 * @param UserActionInterface $user
	 * @param bool $instantlyActivate
	 */
	public function __construct(SaleModel $model, UserActionInterface $user, $instantlyActivate = false)
	{
		$this->currentTime = time();
		$this->model = $model;
		$this->user = $user;

		if ($instantlyActivate) {
			$this->setActivateSale();
		}
	}

	/**
	 * @return mixed
	 */
	public function isActivate()
	{
		return !empty($this->user->promo_step);
	}

	/**
	 * @return mixed
	 */
	public function isComplete()
	{
		return ($this->currentTime >= $this->user->promo_time ||
			$this->currentTime >= $this->user->promo_init_time ||
			$this->user->promo_used >= $this->model->max_used);
	}

	/**
	 * @return mixed
	 */
	public function isNextSale()
	{
		return ($this->currentTime >= $this->user->promo_init_time);
	}

	/**
	 * @return mixed
	 */
	public function isLoop()
	{
		return (($this->user->promo_back >= $this->model->max_back && $this->user->promo_used === 0) ||
			($this->user->promo_used > 0 &&
				$this->user->promo_used < $this->model->max_used &&
				$this->currentTime < $this->user->promo_time &&
				$this->currentTime < $this->user->promo_init_time));
	}

	/**
	 * @return mixed
	 */
	public function isPrev()
	{
		return ($this->user->promo_used === 0 && $this->user->promo_back < $this->model->max_back);
	}

	/**
	 * @return mixed
	 */
	public function isNext()
	{
		return ($this->user->promo_used > 0);
	}

	public function activate()
	{
		if ($this->model->activate === true) {
			$this->setActivateSale();
		} else {
			$params = [
				'name'    => $this->user->promo_name,
				'$or'     => [['date_to' => ['$gte' => time()]], ['date_to' => 0]],
				'enabled' => true,
			];
			$data = SituationCore::oneSituationCheck(new SituationUser($this->user), $this->model, $params, true);
			if (!empty($data)) {
				$this->setActivateSale();
			}
		}
	}

	/**
	 * @param bool $loop
	 */
	public function deactivate($loop = false)
	{
		if ($loop) {
			if ($this->currentTime >= $this->user->promo_init_time) {
				$this->user->promo_time = time() + $this->model->duration;
				$this->user->promo_name = $this->model->name;
				$this->user->promo_step = $this->model->name;
				$this->user->save();
			}
		} else {
			$this->setDeactivateSale();
		}
	}

	/**
	 * @param UserActionInterface $user
	 * @param                     $userPayment
	 */
	public static function buy(UserActionInterface $user, $userPayment)
	{
		if (isset($userPayment['groups'])) {
			if (array_search('promo', $userPayment['groups']) !== false) {
				$user->promo_used++;
			}
			$user->sp_g_pay_after = 0; // TODO Сколько игр было посде последнево платежа
			if (array_search('buy_menu', $userPayment['groups']) !== false) {
				$user->promo_used_menu = $userPayment['price'];
			}
		}
	}

	public function setActivateSale()
	{
		if (isset($this->model->attributes['clean_back'])) {
			$this->user->promo_back = 0;
		}
		$this->user->promo_used = 0;
		$this->user->promo_init_time = strtotime($this->model->transition);
		$this->user->promo_time = time() + $this->model->duration;
		$this->user->promo_name = $this->model->name;
		$this->user->promo_step = $this->model->name;

		$this->model->attributes['user'] = $this->user;
		$this->model->attributes['action'] = $this->model;
		SituationCore::callback($this->model->callback, $this->model->attributes);

		$this->user->save();
	}

	public function setDeactivateSale()
	{
		//		if (isset($this->model->attributes['clean_back']))
		//		{
		//			$this->user->promo_back = 0;
		//		}
		//		$this->user->promo_init_time = 0;
		$this->user->promo_used = 0;
		$this->user->promo_time = 0;
		$this->user->promo_name = $this->nextStep;
		$this->user->promo_step = '';
		$this->user->save();
	}

	/**
	 * @param bool $loop
	 * @return string
	 */
	public function getNextStep($loop = false)
	{
		if ($loop === true) {
			$this->nextStep = $this->model->name;
		} else {
			$used = $this->user->promo_used;
			if (isset($this->model->steps[$used])) {
				$this->nextStep = $this->model->steps[$used];
			} else {
				$this->nextStep = end($this->model->steps);
			}
		}

		return $this->nextStep;
	}
}