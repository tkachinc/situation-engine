<?php
namespace TkachInc\SituationSale\Model;

use TkachInc\SituationalEngine\BaseSituationConfig;

/**
 * Модель конфигурации ситуаций пользователя
 *
 * @property string _id
 * @property string name
 * @property string type
 * @property int duration
 * @property string transition
 * @property int priority
 * @property bool enabled
 * @property bool node
 * @property int max_used
 * @property int max_back
 * @property string class
 * @property string model
 * @property array params
 * @property array attributes
 * @property array callback
 * @property string comments
 * @property mixed steps
 * @property mixed activate
 * @property mixed packResetTime
 * @property mixed date_to
 * @property mixed viewType
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SaleModel extends BaseSituationConfig
{
	/** @var string $collection Название модели */
	protected static $collection = 'pk_action_config';
	/** @var array $sort параметры сортировки */
	protected static $sort = ['enabled' => -1, 'priority' => -1];
	/** @var array $indexes индексы коллекции */
	protected static $indexes = [
		[
			'keys' => ['name' => 1, 'enabled' => -1, 'date_to' => 1],
		],
		[
			'keys' => ['enabled' => -1, 'priority' => -1],
		],
		[
			'keys' => ['enabled' => -1, 'date_to' => 1],
		],
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $fieldsDefault = [
		'duration'   => 0,
		'transition' => '',
		'steps'      => [],
		'max_used'   => 0,
		'max_back'   => 0,
		'activate'   => false,
	];

	/** @var array $fieldsValidate типы значений для валидации */
	protected static $fieldsValidate = [
		'duration'   => self::TYPE_UNSIGNED_INT,
		'transition' => self::TYPE_STRING,
		'steps'      => self::TYPE_JSON,
		'max_used'   => self::TYPE_STRING,
		'max_back'   => self::TYPE_STRING,
		'activate'   => self::TYPE_BOOL,
	];

	protected static $fieldsHint = [
		'duration'   => 'Продолжительность акции',
		'transition' => 'Когда переход к следующей акции (формат функции strtotime в PHP)',
		'steps'      => 'Шаги. 0 (первый элемент) - акция если не купил, 1 - если купил одну покупку, 2 - две покупки, и т.д.',
		'max_used'   => 'Максимально возможное количетсво использований',
		'max_back'   => 'Максмильное количетсво шагов назад',
		'activate'   => 'Активируеться отдельным методом',
	];
}